### Git 使用说明
1.先表明身份
```
	git config --global user.name "Epawn"
	git config --global user.email "1349006778@qq.com"
```
2.从远程clone仓库到本地，并创建自己分支
```
	git clone https://gitlab.com/Epawn/HelloWorld.git
	...
	
```
3.对编辑过的文件进行操作
```
	git add fileName

	git commit -m "annotation"
	
	git push -u origin master
```